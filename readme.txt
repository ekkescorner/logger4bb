logging4bb is a background app and will be started automatically.
logging4bb logs events to a remote socket logger (Lilith)
and/or to a local SQLite database.

there's  another project (hello_logging4bb) as an example how you can use it or customize it.
to understand how to work with logging4bb it's recommended to install the project to learn from the example and to see how easy it is.

soon there will be also a product available at AppWorld with some more comfort using a LogManager4BB UI application

-----------
logging4bb uses Git to manage different platforms,
so the master in this case only has this readme

please checkout the branch you need:

dev-public is always the newest one - at the moment BB OS 7
os6/dev-public is based on BB OS 6

Attention: logging4bb needs as minimum OS 6 and a SD Card.
also: logging4bb isn't running out of the box, because we're using BIS.
RIM doesn't allow to publish the connection string, so you have to edit this.
inside Logging4BBApp the connection string is "????????????" - please replace this with the BIS connection string.
best is to create a local branch for this.
if you don't have BIS, then you can also use deviceside=true for a direct socket connection - but we only support the BIS version.
BlackBerry Forums will help about this.
or you can use the logger available from AppWorld as a product using BIS.

-----------
More Informations ?
http://ekkesapps.wordpress.com/oss/blackberry/logging4bb/

� 2012 ekkehard gentz (ekke)